package com.telerikacademy.api.tests;

public class Constants {

    public static final String KEY = "api_key";
    public static final String VALUE = "special-key";

    public static final String BASE_URL = "https://petstore.swagger.io/v2";

    public static Integer USER_ID;
    public static final String FIRSTNAME = "Isaac";
    public static final String LASTNAME = "Torp";
    public static final String EMAIL_SAMPLE = "katerinna.kostova@gmail.com";
    public static final String PHONE_NUMBER = "957-300-2612";


    public static Integer PET_ID;
    public static final String DOG_NAME = "Djoker";
    public static final String TAG_NAME = "Big Doggie";



}






