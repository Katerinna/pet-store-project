package com.telerikacademy.api.tests;

public class Endpoints {

    public static final String CREATE_USER_ENDPOINT = "/user";
    public static final String GET_SINGLE_USER = "/user/";
    public static final String LOGIN_USER = "/user/login";
    public static final String LOGOUT_USER =  "/user/logout";
    public static final String ADD_NEW_PET = "/pet";
    public static final String GET_PET = "/pet/";

}
