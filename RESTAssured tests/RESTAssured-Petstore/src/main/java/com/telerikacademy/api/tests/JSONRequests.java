package com.telerikacademy.api.tests;

public class JSONRequests {

    public static final String CREATE_NEW_USER = "{\n" +
            "  \"id\": 900504,\n" +
            "  \"username\": \"%s\",\n" +
            "  \"firstName\": \"%s\",\n" +
            "  \"lastName\": \"%s\",\n" +
            "  \"email\": \"%s\",\n" +
            "  \"password\": \"%s\",\n" +
            "  \"phone\": \"%s\",\n" +
            "  \"userStatus\": 2\n" +
            "}";

    public static final String ADD_A_PET = "{\n" +
            "  \"id\": 101027,\n" +
            "  \"category\": {\n" +
            "    \"id\": 1,\n" +
            "    \"name\": \"%s\"\n" +
            "  },\n" +
            "  \"name\": \"doggie\",\n" +
            "  \"photoUrls\": [\n" +
            "    \"string\"\n" +
            "  ],\n" +
            "  \"tags\": [\n" +
            "    {\n" +
            "      \"id\": 1,\n" +
            "      \"name\": \"%s\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"status\": \"available\"\n" +
            "}";
}
