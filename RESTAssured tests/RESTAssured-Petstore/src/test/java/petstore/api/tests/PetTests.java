package petstore.api.tests;

import base.BaseTestSetup;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static com.telerikacademy.api.tests.Constants.*;
import static com.telerikacademy.api.tests.Endpoints.*;
import static com.telerikacademy.api.tests.Helper.isValid;
import static com.telerikacademy.api.tests.JSONRequests.ADD_A_PET;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class PetTests extends BaseTestSetup {
    @Test(priority = 1)
    public void addNewPetTest(){

        baseURI = format ("%s%s",BASE_URL, ADD_NEW_PET);

        String requestBody = (format(ADD_A_PET,DOG_NAME,TAG_NAME));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response = given()
                .auth().basic(KEY,VALUE)
                .contentType("application/json")
                .body(requestBody)
                .when()
                .post();

        int statusCode = response.getStatusCode();
        String contentType = response.getContentType();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");
        assertEquals(contentType,"application/json","Content-type is not application/json");

        PET_ID = response.getBody().jsonPath().getInt("id");

        System.out.printf("Pet with name %s and ID %d was added in the system.%n",DOG_NAME,PET_ID);
    }

    @Test(priority = 2)
    public void getPetByIdTest(){

        baseURI = format ("%s%s%s",BASE_URL, GET_PET,PET_ID);

        Response response = given()
                .auth().basic(KEY,VALUE)
                .when()
                .get();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");
        assertEquals(response.getBody().jsonPath().get("category.name"),DOG_NAME,
                "Dog name  doesn't match.");
    }

    @Test(priority = 3)
    public void deletePetTest(){

        baseURI = format ("%s%s%s",BASE_URL, GET_PET,PET_ID);

        Response response = given()
                .auth().basic(KEY,VALUE)
                .when()
                .get();

        int statusCode = response.getStatusCode();
        assertTrue(statusCode == 200 || statusCode == 404, "Incorrect status code. Expected 200 or 404.");

        System.out.printf("Pet with name %s and ID %d was deleted.",DOG_NAME,PET_ID);
    }
}
