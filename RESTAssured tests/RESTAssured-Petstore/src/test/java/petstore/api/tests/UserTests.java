package petstore.api.tests;

import base.BaseTestSetup;
import io.restassured.response.Response;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import static com.telerikacademy.api.tests.Constants.*;
import static com.telerikacademy.api.tests.Endpoints.*;
import static com.telerikacademy.api.tests.Helper.isValid;
import static com.telerikacademy.api.tests.JSONRequests.CREATE_NEW_USER;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class UserTests extends BaseTestSetup {

    private final String userNameBase = "User";
    private String randomStr = RandomStringUtils.randomAlphabetic(7);
    private String username = userNameBase.concat(randomStr);
    private String password = RandomStringUtils.randomAlphanumeric(15);

    @Test(priority = 1)

    public void createNewUserTest(){

        baseURI = format ("%s%s",BASE_URL, CREATE_USER_ENDPOINT);

        String requestBody = (format(CREATE_NEW_USER,username,FIRSTNAME,LASTNAME,EMAIL_SAMPLE,password,PHONE_NUMBER));
        assertTrue(isValid(requestBody), "Body is not a valid JSON");

        Response response = given()
                .contentType("application/json")
                .body(format(requestBody))
                .when()
                .post();

        int statusCode = response.getStatusCode();
        String contentType = response.getContentType();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");
        assertEquals(contentType,"application/json","Content-type is not application/json");

        USER_ID = response.getBody().jsonPath().getInt("message");

        System.out.printf("User with username %s and ID %d was created.%n",username,USER_ID);

    }

    @Test(priority = 2)
    public void getSingleUserByUsernameTest(){

        baseURI = format ("%s%s%s",BASE_URL, GET_SINGLE_USER,username);

        Response response = given()
                .when()
                .get();

        int statusCode = response.getStatusCode();
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");
        assertEquals(response.getBody().jsonPath().getString("username"),username,
                "Username doesn't match.");
    }

    @Test (priority = 3)
    public void loginUserTest(){
        baseURI = format ("%s%s",BASE_URL, LOGIN_USER);

        Response response = given()
                .queryParam("username", username)
                .queryParam("password", password)
                .when()
                .get();

        int statusCode = response.getStatusCode();
        String message = response.getBody().jsonPath().getString("message");
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");
        assertEquals(message.contains("logged in user session:"), true, "User is not logged with valid session");

        System.out.print("User was logged in successfully.%n");
    }

    @Test (priority = 4)
    public void logoutUserTest(){

        baseURI = format ("%s%s",BASE_URL, LOGOUT_USER);

        Response response = given()
                .when()
                .get();

        int statusCode = response.getStatusCode();
        String message = response.getBody().jsonPath().getString("message");
        assertEquals(statusCode, 200, "Incorrect status code. Expected 200.");
        assertEquals(message,"ok","User is not logout");

        System.out.print("User was logged out successfully.%n");
    }

    @Test (priority = 5)
    public void deleteUserTest(){

        baseURI = format ("%s%s%s",BASE_URL, GET_SINGLE_USER,username);

        Response response = given()
                .when()
                .delete();

        int statusCode = response.getStatusCode();
        assertTrue(statusCode == 200 || statusCode == 404, "Incorrect status code. Expected 200 or 404.");

        System.out.printf("User with username %s and ID %d was deleted.",username,USER_ID);
    }
}